<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stocklist extends Model
{
    protected $table = 'stocklists';
    public $primaryKey= 'id';
    public $timestamps = true;
}
