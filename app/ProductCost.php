<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCost extends Model
{
    protected $table = 'product_costs';
    public $primaryKey= 'id';
    public $timestamps = true;

    public function ProductStock(){
        return $this->belongsTo('App\ProductStock');
    }
}
