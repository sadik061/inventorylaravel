<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{
    protected $table = 'accounts';
    public $primaryKey= 'id';
    public $timestamps = true;
}
