<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class temp extends Model
{
    protected $table = 'temp';
    public $primaryKey= 'id';
    public $timestamps = true;

}
