<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Brand;
use App\Category;
use App\Stocklist;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Product = Product::all();
        $Brand = Brand::all();
        $Category = Category::all();
        $Stocklist = Stocklist::all();
        $clist = array();
        $blist = array();
        $slist = array();
        foreach ($Category as $c)
        {
            $clist[$c->id] = $c->Category_name;
        }
        foreach ($Brand as $c)
        {
            $blist[$c->id] = $c->name;
        }
        foreach ($Stocklist as $c)
        {
            $slist[$c->id] = $c->stock_list_name;
        }


        return view('product.index')->with('Product',$Product)->with('blist',$blist)->with('clist',$clist)->with('slist',$slist)->with('Brand',$Brand)->with('Category',$Category);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'Product' => 'required'
        ]);
        $b = new Product();
        $b->name = $request->input('Product');
        $b->Stocklist_id = $request->input('Stock');
        $b->Category_id = $request->input('category');
        $b->Brand_id = $request->input('Brand');
        $b->save();
        return redirect('/product')->with('success','Product Created');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $Product = Product::all();
        $editt = Product::all()->find($id);
        $Brand = Brand::all();
        $Category = Category::all();
        $Stocklist = Stocklist::all();
        $clist = array();
        $blist = array();
        $slist = array();
        foreach ($Category as $c)
        {
            $clist[$c->id] = $c->Category_name;
        }
        foreach ($Brand as $c)
        {
            $blist[$c->id] = $c->name;
        }
        foreach ($Stocklist as $c)
        {
            $slist[$c->id] = $c->stock_list_name;
        }
        return view('product.edit')->with('Product',$Product)->with('blist',$blist)->with('clist',$clist)->with('slist',$slist)->with('Brand',$Brand)->with('Category',$Category)->with('editt', $editt);

    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'Product' => 'required',
        ]);
        $b = Product::find($id);
        $b->name = $request->input('Product');
        $b->Stocklist_id = $request->input('Stock');
        $b->Category_id = $request->input('category');
        $b->Brand_id = $request->input('Brand');
        $b->save();
        return redirect('/product')->with('success','product Updated');
    }


    public function destroy($id)
    {
        $Product = Product::find($id);
        $Product->delete();
        return redirect('/product')->with('success','Product Deleted');
    }
}
