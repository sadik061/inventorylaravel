<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Business;

class BusniessCostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $Business = Business::all();
        return view('business.index')->with('Business',$Business);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //handel file upload
        if($request->hasFile('brand_logo')){
            //get filename with the extension
            $FilenameWithExt = $request->file('brand_logo')->getClientOriginalName();
            //get just filenam
            $Filename = pathinfo($FilenameWithExt, PATHINFO_FILENAME);
            //get just extension
            $extension = $request->file('brand_logo')->getClientOriginalExtension();
            //file name to store
            $fileNametoStore = $Filename.'_'.time().'_'.$extension;
            //upload image
            $path = $request->file('brand_logo')->storeAs('public/brand_logo',$fileNametoStore);
        }else{
            $fileNametoStore = 'noimage.jpg';
        }
        $b = Business::find($id);
        $b->id = $request->input('id');
        $b->name = $request->input('Name');
        $b->email = $request->input('Email');
        $b->city = $request->input('City');
        $b->country = $request->input('Country');
        $b->address = $request->input('Address');
        $b->contact = $request->input('contact');
        $b->business_logo = $fileNametoStore;
        $b->save();
        return redirect('/business')->with('success','Business info Updated');
    }


    public function destroy($id)
    {
        //
    }
}
