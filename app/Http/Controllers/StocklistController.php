<?php

namespace App\Http\Controllers;
use App\Stocklist;

use Illuminate\Http\Request;

class StocklistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $Stocklist = Stocklist::all();
        return view('stocklist.index')->with('Stocklist',$Stocklist);
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required'
        ]);
        $b = new Stocklist();
        $b->stock_list_name = $request->input('title');
        $b->save();
        return redirect('/stocklist')->with('success','Sock Created');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $Stocklist = Stocklist::all();
        $editt = Stocklist::all()->find($id);
        return view('stocklist.edit')->with('Stocklist',$Stocklist)->with('editt', $editt);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'edittitle' => 'required'
        ]);
        $b = Stocklist::find($id);
        $b->stock_list_name = $request->input('edittitle');
        $b->save();
        return redirect('/stocklist')->with('success','Stock name Updated');
    }


    public function destroy($id)
    {
        $Category = Stocklist::find($id);
        $Category->delete();
        return redirect('/stocklist')->with('success','Stock Deleted');
    }
}
