<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\temp;
use App\ProductStock;
use App\Product;
use App\Stocklist;
use App\Category;
use App\Brand;
use App\Contact;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function Checkout()
    {
        $Category = Category::all();
        $product = Product::all();
        $Brand = Brand::all();
        $Stocklist = Stocklist::all();
        $ProductStock = ProductStock::all();
        $temp = temp::all();
        $subtotal=0;
        $discount=0;
        foreach ($temp as $b)
        {
            foreach ($ProductStock as $s)
            {
                if($b->sotck_id == $s->id){
                    $subtotal+=$subtotal+$s->Sale*$b->amount;
                    $discount+=$b->discount;
                }
            }
        }
        $Contact = Contact::all();
        $clist = array();
        foreach ($Contact as $c)
        {
            if($c->Type=="Customer") {
                $clist[$c->id] = $c->name;
            }
        }
        return view('invoice.sell')->with('temp',$temp)->with('Category',$Category)->with('product',$product)->with('Brand',$Brand)->with('ProductStock',$ProductStock)->with('Stocklist',$Stocklist)->with('subtotal',$subtotal)->with('discount',$discount)->with('clist',$clist);
    }

    public function getcontactphone(Request $request)
    {
        $Contact = Contact::all()->find($request->id);
        return response()->json($Contact->contact);


    }
}
