<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductStock;
use App\Product;
use App\Brand;
use App\Stocklist;
use App\Category;

class ProductStockController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $ProductStock = ProductStock::all();
        $Product = Product::all();
        //$editt = Product::all()->find($id);
        $Brand = Brand::all();
        $Category = Category::all();
        $Stocklist = Stocklist::all();
        $plist = array();
        $slist = array();
        foreach ($Stocklist as $c)
        {
            $slist[$c->id] = $c->stock_list_name;
        }
        foreach ($Product as $c)
        {
            $plist[$c->id] = $c->name;
        }
        return view('productstock.index')->with('ProductStock',$ProductStock)->with('Brand',$Brand)->with('Category',$Category)->with('Stocklist',$Stocklist)->with('Product', $Product)->with('plist', $plist)->with('slist', $slist);

    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'Quantity' => 'required'
        ]);
        $b = new ProductStock();
        $b->Product_id = $request->input('Product');
        $b->Stock_list_id = $request->input('Stock');
        $b->Purchase = $request->input('Purchase');
        $b->Sale = $request->input('Sale');
        $b->Quantity = $request->input('Quantity');
        $b->save();
        return redirect('/productstock')->with('success','Product Stock Created');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $editt = ProductStock::all()->find($id);
        $ProductStock = ProductStock::all();
        $Product = Product::all();
        $Brand = Brand::all();
        $Category = Category::all();
        $Stocklist = Stocklist::all();
        $plist = array();
        $slist = array();
        foreach ($Stocklist as $c)
        {
            $slist[$c->id] = $c->stock_list_name;
        }
        foreach ($Product as $c)
        {
            $plist[$c->id] = $c->name;
        }
        return view('productstock.edit')->with('ProductStock',$ProductStock)->with('Brand',$Brand)->with('Category',$Category)->with('Stocklist',$Stocklist)->with('Product', $Product)->with('plist', $plist)->with('slist', $slist)->with('editt',$editt)->with('Cost',$editt->ProductCost);
    }

    public function update(Request $request, $id)
    {

        $b = ProductStock::find($id);
        $b->Product_id = $request->input('Product');
        $b->Stock_list_id = $request->input('Stock');
        $b->Purchase = $request->input('Purchase');
        $b->Sale = $request->input('Sale');
        $b->Quantity = $request->input('Quantity');
        $b->save();
        return  redirect()->back()->with('success','product stock Updated');
    }

    public function destroy($id)
    {
        $ProductStock = ProductStock::find($id);
        $ProductStock->delete();
        return redirect('/productstock')->with('success','Product stock Deleted');
    }
}
