<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accounts;

class AccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $Accounts = Accounts::all();
        return view('accounts.index')->with('Accounts',$Accounts);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'Name' => 'required',
            'Type'=>'required'
        ]);
        $b = new Accounts();
        $b->Business_id = $request->input('Business_id');
        $b->Type = $request->input('Type');
        $b->name = $request->input('Name');
        $b->save();
        return redirect('/accounts')->with('success','Account created');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $editt = Accounts::all()->find($id);
        return view('accounts.edit')->with('editt',$editt);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'Name' => 'required',
            'Type'=>'required'
        ]);
        $b = Accounts::find($id);
        $b->Business_id = $request->input('Business_id');
        $b->Type = $request->input('Type');
        $b->name = $request->input('Name');
        $b->save();
        return redirect('/accounts')->with('success','Account Updated');
    }


    public function destroy($id)
    {
        $Accounts = Accounts::find($id);
        $Accounts->delete();
        return redirect('/accounts')->with('success','Account Deleted');
    }
}
