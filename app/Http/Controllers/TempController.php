<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\temp;
use App\ProductStock;
use App\Product;
use App\Stocklist;
use App\Category;
use App\Brand;

class TempController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index()
    {

        $Category = Category::all();
        $product = Product::all();
        $Brand = Brand::all();
        $Stocklist = Stocklist::all();
        $ProductStock = ProductStock::all();
        $clist = array();
        $blist = array();
        foreach ($Category as $c)
        {
            $clist[$c->id] = $c->Category_name;
        }
        foreach ($Brand as $b)
        {
            $blist[$b->id] = $b->name;
        }
        $temp = temp::all();
        return view('temp.index')->with('temp',$temp)->with('Category',$Category)->with('clist',$clist)->with('blist',$blist)->with('product',$product)->with('Brand',$Brand)->with('ProductStock',$ProductStock)->with('Stocklist',$Stocklist);
    }

    public function findproductname(Request $request){
        $data= Product::select('name','id')->where('Category_id',$request->id)->take(100)->get();
        return response()->json($data);
    }
    public function findstock(Request $request){
        $d =  DB::table('product_stocks')
            ->join('stocklists', 'stocklists.id', '=', 'product_stocks.Stock_list_id')
            ->select('stocklists.*')
            ->where('product_stocks.Product_id',$request->id)
            ->get();

        return response()->json($d);
    }
    public function findstockbylocation(Request $request){
        $data= ProductStock::select('Sale','Quantity')->where('Product_id',$request->p_id)->where('Stock_list_id',$request->id)->take(100)->get();
        return response()->json($data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'amount' => 'required'
        ]);
        $b = new temp();
        $b->item_id = $request->input('prod_id');
        $b->brand_id = $request->input('brand_id');
        $b->category_id = $request->input('cat_id');
        $b->sotck_id = $request->input('loca_id');
        $b->amount = $request->input('amount');
        $b->discount = $request->input('discount');

        $b->save();
        return redirect('/temp')->with('success','Product Stock Created');
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $temp = temp::find($id);
        $temp->delete();
        return redirect('/temp')->with('success','Product Deleted');
    }
}
