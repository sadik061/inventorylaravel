<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use PhpParser\Node\Scalar\String_;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $Category = Category::all();
        $a = array();
        foreach ($Category as $c)
        {
            $a[$c->id] = $c->Category_name;
        }
        return view('category.index')->with('Category',$Category)->with('parentlist',$a);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'Caterogy_Name' => 'required'
        ]);
        $b = new Category();
        $b->parent_id = $request->input('Parentname');
        $b->Category_name = $request->input('Caterogy_Name');
        $b->save();
        return redirect('/category')->with('success','Category Created');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $Category = Category::all();
        $editt = Category::all()->find($id);
        $a = array();
        foreach ($Category as $c)
        {
            $a[$c->id] = $c->Category_name;
        }
        return view('category.edit')->with('Category',$Category)->with('editt', $editt)->with('parentlist',$a);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'Category_name' => 'required',
            'Parentname' => 'required'
        ]);
        $b = Category::find($id);
        $b->Category_name = $request->input('Category_name');
        $b->Parent_id = $request->input('Parentname');
        $b->save();
        return redirect('/category')->with('success','Category Updated');
    }


    public function destroy($id)
    {
        $Category = Category::find($id);
        $Category->delete();
        return redirect('/category')->with('success','Category Deleted');
    }
}
