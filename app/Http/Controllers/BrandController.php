<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $brand = Brand::all();
        return view('brand.index')->with('Brand',$brand);
    }



    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required'
        ]);
        $b = new Brand;
        $b->name = $request->input('title');
        $b->save();
        return redirect('/Brand')->with('success','Brand Created');
    }


    public function show($id)
    {

    }

    public function edit($id)
    {
        $brand = Brand::all();
        $editt = Brand::all()->find($id);
        return view('brand.edit')->with('Brand',$brand)->with('editt', $editt);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'edittitle' => 'required'
        ]);
        $b = Brand::find($id);
        $b->name = $request->input('edittitle');
        $b->save();
        return redirect('/Brand')->with('success','Brand Updated');
    }

    public function destroy($id)
    {
        $brand = Brand::find($id);
        $brand->delete();
        return redirect('/Brand')->with('success','Brand Deleted');

    }
}
