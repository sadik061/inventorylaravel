<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCost;
use App\ProductStock;
use Illuminate\Support\Facades\DB;

class ProductCostController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'Amount' => 'required',
            'Name' => 'required'
        ]);



        DB::table('product_stocks')->where('id', $request->input('product_stock_id'))->increment('Purchase', $request->input('Amount'));
        $b = new ProductCost();
        $b->product_stock_id = $request->input('product_stock_id');
        $b->name = $request->input('Name');
        $b->descripttion = $request->input('Description');
        $b->amount = $request->input('Amount');
        $b->save();
        return  redirect()->back()->with('success','New Cost Added');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $ProductCost = ProductCost::find($id);
        DB::table('product_stocks')->where('id', $ProductCost->product_stock_id)->increment('Purchase', (-1*$ProductCost->amount));
        $ProductCost->delete();
        return  redirect()->back()->with('success','Cost has been deleted');
    }
}
