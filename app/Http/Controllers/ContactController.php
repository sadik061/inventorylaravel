<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $Contact = Contact::all();
        return view('contact.index')->with('Contact',$Contact);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        if($request->hasFile('brand_logo')){
            //get filename with the extension
            $FilenameWithExt = $request->file('brand_logo')->getClientOriginalName();
            //get just filenam
            $Filename = pathinfo($FilenameWithExt, PATHINFO_FILENAME);
            //get just extension
            $extension = $request->file('brand_logo')->getClientOriginalExtension();
            //file name to store
            $fileNametoStore = $Filename.'_'.time().'_'.$extension;
            //upload image
            $path = $request->file('brand_logo')->storeAs('public/brand_logo',$fileNametoStore);
        }else{
            $fileNametoStore = 'noimage.jpg';
        }
        $this->validate($request,[
            'Name' => 'required',
            'Email'=>'required',
            'Address'=>'required',
            'contact'=>'required',
            'Type'=>'required'
        ]);
        $b = new Contact();
        $b->Business_id = $request->input('Business_id');
        $b->Type = $request->input('Type');
        $b->name = $request->input('Name');
        $b->email = $request->input('Email');
        $b->address = $request->input('Address');
        $b->city = $request->input('City');
        $b->country = $request->input('Country');
        $b->contact = $request->input('contact');
        $b->business_logo = $fileNametoStore;
        $b->save();
        return redirect('/contact')->with('success','Contact created');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $editt = Contact::all()->find($id);
        return view('contact.edit')->with('editt',$editt);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'Name' => 'required',
            'Email'=>'required',
            'Address'=>'required',
            'contact'=>'required',
            'Type'=>'required'
        ]);
        if($request->hasFile('brand_logo')){
            //get filename with the extension
            $FilenameWithExt = $request->file('brand_logo')->getClientOriginalName();
            //get just filenam
            $Filename = pathinfo($FilenameWithExt, PATHINFO_FILENAME);
            //get just extension
            $extension = $request->file('brand_logo')->getClientOriginalExtension();
            //file name to store
            $fileNametoStore = $Filename.'_'.time().'_'.$extension;
            //upload image
            $path = $request->file('brand_logo')->storeAs('public/brand_logo',$fileNametoStore);
        }else{
            $fileNametoStore = 'noimage.jpg';
        }
        $b = Contact::find($id);
        $b->Business_id = $request->input('Business_id');
        $b->Type = $request->input('Type');
        $b->name = $request->input('Name');
        $b->email = $request->input('Email');
        $b->address = $request->input('Address');
        $b->city = $request->input('City');
        $b->country = $request->input('Country');
        $b->contact = $request->input('contact');
        $b->business_logo = $fileNametoStore;
        $b->save();
        return redirect('/contact')->with('success','Contact Updated');
    }


    public function destroy($id)
    {
        $ProductStock = Contact::find($id);
        $ProductStock->delete();
        return redirect('/contact')->with('success','Contact Deleted');
    }
}
