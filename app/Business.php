<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $table = 'businesses';
    public $primaryKey= 'id';
    public $timestamps = true;
}
