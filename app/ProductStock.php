<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    protected $table = 'product_stocks';
    public $primaryKey= 'id';
    public $timestamps = true;

    public function ProductCost(){
        return $this->hasMany('App\ProductCost');
    }
}
