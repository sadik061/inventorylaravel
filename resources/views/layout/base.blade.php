<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Inventory</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/zabuto_calendar.css')}}" rel="stylesheet">
    <link href="{{asset('js/gritter/css/jquery.gritter.css')}}" rel="stylesheet">
    <link href="{{asset('lineicons/style.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/custome.css')}}" rel="stylesheet">
    <link href="{{asset('css/style-responsive.css')}}" rel="stylesheet">

    <script src="{{asset('js/chart-master/Chart.js')}}"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <!-- <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" rel="text/javascript" ></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" rel="text/javascript"></script>
    <![endif]-->
</head>
<body>
<section id="container" >
    @include('includes.navbar')
    <section id="main-content">
        <section class="wrapper">
            @include('includes.messages')
            @yield('content')



            <script rel="text/javascript" src="{{asset('js/jquery.js')}}"></script>
            <script rel="text/javascript"src="{{asset('js/jquery-1.8.3.min.js')}}"></script>
            <script src="{{asset('js/app.js')}}"></script>
            <script src="{{asset('js/jquery.dcjqaccordion.2.7.js')}}"></script>
            <script src="{{asset('js/jquery.scrollTo.min.js')}}"></script>
            <script src="{{asset('js/jquery.nicescroll.js')}}"></script>
            <script src="{{asset('js/jquery.sparkline.js')}}"></script>


            <!--common script for all pages-->
            <script src="{{asset('js/common-scripts.js')}}"></script>

            <script src="{{asset('js/jquery.gritter.js')}}"></script>
            <script src="{{asset('js/gritter-conf.js')}}"></script>
        </section>
    </section>
</section>
</body>