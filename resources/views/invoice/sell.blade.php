@extends('layout.base')
@section('content')
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Order Details</h4>
            {!! Form::open(['action' => 'ContactController@store', 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
                <div class="form-group ">
                    <label class="col-sm-2 col-sm-2 control-label">Customer name</label>
                    <div class="col-sm-4">
                        {{Form::select('contact',$clist,'""',['class'=>'form-control contact','placeholder' => 'Pick a Contact...'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Contact</label>
                    <div class="col-sm-4" id="contactt">
                        <input type="text" class="form-control contactt" value="" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Sales order Date</label>
                    <div class="col-sm-2">
                        {{Form::date('order', \Carbon\Carbon::now(),['class'=>'form-control'])}}
                    </div>
                    <label class="col-sm-2 col-sm-2 control-label">Expected Shipment Date</label>
                    <div class="col-sm-2">
                        {{Form::date('shipment', \Carbon\Carbon::now(),['class'=>'form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Payment Method</label>
                    <div class="col-sm-2">
                        {{Form::select('Method', ['Cash' => 'Cash', 'Bank' => 'Bank'], null, ['placeholder' => 'Pick a method...','class'=>'form-control'])}}
                    </div>
                </div>
                <h4><i class="fa fa-angle-right"></i> purchased products</h4>
                <section id="unseen">
                    <table id="table" class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Category</th>
                            <th>Item</th>
                            <th>Brand</th>
                            <th class="numeric">Price</th>
                            <th class="numeric">amount</th>
                            <th class="numeric">discount</th>
                            <th class="numeric"style="padding-left:  2%;padding-right: 4%;">Total</th>
                        </tr>
                        </thead>
                        <tbody id="data" >
                        @if(count($temp) >= 1)
                            @foreach($temp as $b)
                                <tr>
                                    <td>
                                        @foreach($Category as $c)
                                            @if($b->category_id==$c->id)
                                                {{$c->Category_name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>@foreach($product as $p)
                                            @if($b->item_id==$p->id)
                                                {{$p->name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($Brand as $B)
                                            @if($b->brand_id==$B->id)
                                                {{$B->name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($ProductStock as $s)
                                            @if($b->sotck_id==$s->id)
                                                {{$s->Sale}}

                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{$b->amount}}</td>
                                    <td>{{$b->discount}}</td>
                                    <td>
                                        @foreach($ProductStock as $s)
                                            @if($b->sotck_id==$s->id)
                                                {{$s->Sale*$b->amount-$b->discount}}

                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr><td><mark>no Brands in the list</mark></td></tr>
                        @endif
                        </tbody>
                    </table>

                </section>

                <div class="form-group">
                    <label class="col-sm-6 col-sm-6 control-label"></label>
                    <label class="col-lg-2 col-sm-2 control-label">Sub total</label>
                    <div class="col-sm-2">
                        <p class="form-control-static">{{$subtotal}}</p>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-6 col-sm-6 control-label"></label>
                    <label class="col-sm-2 col-sm-2 control-label">Discount</label>
                    <div class="col-sm-2">
                        <p class="form-control-static">{{$discount}}</p>
                    </div>
                </div>

                <div class="form-group" style=" background-color: #f9f9f9;">
                    <label class="col-sm-6 col-sm-6 control-label"></label>
                    <label class="col-lg-2 col-sm-2 control-label" style=" background-color: #f9f9f9;">Total</label>
                    <div class="col-sm-2" style=" background-color: #f9f9f9;">
                        <p class="form-control-static">{{$subtotal-$discount}}</p>
                    </div>
                </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Payment</label>
                <div class="col-sm-2">
                    {{Form::number('name','',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Payment Status</label>
                <div class="col-sm-2">
                    {{Form::select('Method', ['Due' => 'Due', 'Full' => 'Full', 'Partial' => 'Partial'], null, ['placeholder' => 'Pick a status...','class'=>'form-control'])}}
                </div>
            </div >
                <div class="form-group">
                    <label class="col-sm-6 col-sm-6 control-label" for="comment">Comment:</label>
                    <textarea class="form-control col-sm-10 col-sm-10 control-label" rows="5" id="comment"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Order</button>
            </form>

        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $(document).on('change', '.contact', function () {
            var id = $(this).val();
            $.ajax({
                type: 'get',
                url: '{!! URL::to('getcontactphone') !!}',
                data: {'id': id},
                success: function (data) {
                    console.log(data);
                    var op="";
                    op+= '<input type="text" class="form-control" disabled value="'+data+'">';
                    document.getElementById("contactt").innerHTML = op;
                },
                error: function () {

                }

            });
        });
    });
</script>
@endsection
