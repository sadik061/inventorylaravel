@extends('layout.base')
@section('content')
    <div class="row mt">
        <div class="col-md-12">
            <script>
                $('#myTabs a').click(function (e) {
                    e.preventDefault()
                    $(this).tab('show')
                })
                $('#myTabs a[href="#Addnew"]').tab('show') // Select tab by name

            </script>
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Addnew" aria-controls="Addnew" role="tab" data-toggle="tab">Update</a></li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="Addnew">
                        <div class="form-panel">
                            <h3><i class="fa fa-angle-right"></i> Account Info </h3>
                            <div class="col-sm-12">

                            </div>
                            {!! Form::open(['action' => ['AccountsController@update',$editt->id], 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7; margin-top: 2%;">
                                {{Form::label('Type', 'Type',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::select('Type', ['Liability' => 'Liability', 'Asset' => 'Asset','Expense' => 'Expense'], null, ['placeholder' => 'Pick a type...','class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style="display: none; border-bottom: 0px solid #eff2f7;">
                                {{Form::label('id', 'Business id',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Business_id',$editt->Business_id,['class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7; margin-top: 2%;">
                                {{Form::label('Name', 'Name',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Name',$editt->name,['class'=>'form-control'])}}
                                </div>

                            </div>



                            {{form::hidden('_method','PUT')}}
                            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
                            {!! Form::close() !!}


                        </div>
                    </div>



@endsection