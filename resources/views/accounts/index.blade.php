@extends('layout.base')
@section('content')
    <div class="row mt">
        <div class="col-md-12">
            <script>
                $('#myTabs a').click(function (e) {
                    e.preventDefault()
                    $(this).tab('show')
                })
                $('#myTabs a[href="#Addnew"]').tab('show') // Select tab by name
                $('#myTabs a[href="#Accountlist"]').tab('show') // Select tab by name

            </script>
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Addnew" aria-controls="Addnew" role="tab" data-toggle="tab">Add New</a></li>
                    <li role="presentation"><a href="#Accountlist" aria-controls="list" role="tab" data-toggle="tab">Accounts</a></li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="Addnew">
                        <div class="form-panel">
                            <h3><i class="fa fa-angle-right"></i> Account Info </h3>
                            <div class="col-sm-12">

                            </div>
                            {!! Form::open(['action' => 'AccountsController@store', 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7; margin-top: 2%;">
                                {{Form::label('Type', 'Type',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::select('Type', ['Liability' => 'Liability', 'Asset' => 'Asset','Expense' => 'Expense'], null, ['placeholder' => 'Pick a type...','class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style="display: none; border-bottom: 0px solid #eff2f7;">
                                {{Form::label('id', 'Business id',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Business_id',1,['class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7; margin-top: 2%;">
                                {{Form::label('Name', 'Name',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Name',"",['class'=>'form-control'])}}
                                </div>

                            </div>



                            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
                            {!! Form::close() !!}


                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane" id="Accountlist"><div class="content-panel">
                            <table class="table table-striped table-advance table-hover">
                                <h4><i class="fa fa-angle-right"></i>Accounts</h4>
                                <thead>
                                <tr>
                                    <th>name</th>
                                    <th>Type</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if(count($Accounts) >= 1)
                                    @foreach($Accounts as $b)

                                            <tr>
                                                <td>{{$b->name}}</td>
                                                <td>{{$b->type}}</td>

                                                <td>
                                                    {!! Form::open(['action' => ['AccountsController@destroy',$b->id], 'method'=>'POST']) !!}
                                                    <a class="editt btn btn-primary btn-xs" href="/accounts/{{$b->id}}/edit">Edit</a>
                                                    {{Form::hidden('_method','DELETE')}}
                                                    {{form::submit('Delete',['class' => 'btn btn-danger btn-xs'])}}

                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>

                                    @endforeach
                                @else
                                    <tr><td><mark>no accounts in the list</mark></td></tr>
                                @endif

                                </tbody>
                            </table>


                        </div>
                    </div>


@endsection