@extends('layout.base')
@section('content')
    <div class="row mt">
        <div class="col-md-12">
            <script>
                $('#myTabs a').click(function (e) {
                    e.preventDefault()
                    $(this).tab('show')
                })
                $('#myTabs a[href="#Addnew"]').tab('show') // Select tab by name
                $('#myTabs a[href="#Customerlist"]').tab('show') // Select tab by name
                $('#myTabs a[href="#Vendorlist"]').tab('show') // Select tab by name
            </script>
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Addnew" aria-controls="Addnew" role="tab" data-toggle="tab">Add New</a></li>
                    <li role="presentation"><a href="#Customerlist" aria-controls="list" role="tab" data-toggle="tab">Customer List</a></li>
                    <li role="presentation"><a href="#Vendorlist" aria-controls="list" role="tab" data-toggle="tab">Vendor List</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="Addnew">
                        <div class="form-panel">
                            <h3><i class="fa fa-angle-right"></i> Customer Info </h3>
                            <div class="col-sm-12">

                            </div>
                            {!! Form::open(['action' => 'ContactController@store', 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7; margin-top: 2%;">
                                {{Form::label('Type', 'Type',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::select('Type', ['Customer' => 'Customer', 'Vendor' => 'Vendor'], null, ['placeholder' => 'Pick a type...','class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style="display: none; border-bottom: 0px solid #eff2f7;">
                                {{Form::label('id', 'Business id',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Business_id',1,['class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7; margin-top: 2%;">
                                {{Form::label('Name', 'Name',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Name',"",['class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Email', 'Email',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Email',"",['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('City', 'City',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('City',"",['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Country', 'Country',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Country',"",['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Address', 'Contact',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('contact',"",['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Address', 'Address',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-6">
                                    {{Form::text('Address',"",['class'=>'form-control'])}}
                                </div>
                            </div>

                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('logo', 'Brand logo',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-6">
                                    {{Form::file('brand_logo')}}
                                </div>
                            </div>

                            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
                            {!! Form::close() !!}


                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane" id="Vendorlist"><div class="content-panel">
                            <table class="table table-striped table-advance table-hover">
                                <h4><i class="fa fa-angle-right"></i> All Customer</h4>
                                    <thead>
                                    <tr>
                                        <th>name</th>
                                        <th>Email</th>
                                        <th>City</th>
                                        <th>Country</th>
                                        <th>contact</th>
                                        <th>Address</th>
                                        <th>action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($Contact) >= 1)
                                        @foreach($Contact as $b)
                                            @if($b->Type == "Vendor")
                                            <tr>
                                                <td>{{$b->name}}</td>
                                                <td>{{$b->email}}</td>
                                                <td>{{$b->city}}</td>
                                                <td>{{$b->country}}</td>
                                                <td>{{$b->contact}}</td>
                                                <td>{{$b->address}}</td>



                                                <td>
                                                    {!! Form::open(['action' => ['ContactController@destroy',$b->id], 'method'=>'POST']) !!}
                                                    <a class="editt btn btn-primary btn-xs" href="/contact/{{$b->id}}/edit">Edit</a>
                                                    {{Form::hidden('_method','DELETE')}}
                                                    {{form::submit('Delete',['class' => 'btn btn-danger btn-xs'])}}

                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr><td><mark>no Brands in the list</mark></td></tr>
                                    @endif

                                    </tbody>
                                </table>


                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="Customerlist"><div class="content-panel">
                            <table class="table table-striped table-advance table-hover">
                                <h4><i class="fa fa-angle-right"></i> All Vendors</h4>
                                <thead>
                                <tr>
                                    <th>name</th>
                                    <th>Email</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    <th>contact</th>
                                    <th>Address</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($Contact) >= 1)
                                    @foreach($Contact as $b)
                                        @if($b->Type == "Customer")
                                            <tr>
                                                <td>{{$b->name}}</td>
                                                <td>{{$b->email}}</td>
                                                <td>{{$b->city}}</td>
                                                <td>{{$b->country}}</td>
                                                <td>{{$b->contact}}</td>
                                                <td>{{$b->address}}</td>
                                                <td>
                                                    {!! Form::open(['action' => ['ContactController@destroy',$b->id], 'method'=>'POST']) !!}
                                                    <a class="editt btn btn-primary btn-xs" href="/contact/{{$b->id}}/edit">Edit</a>
                                                    {{Form::hidden('_method','DELETE')}}
                                                    {{form::submit('Delete',['class' => 'btn btn-danger btn-xs'])}}

                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr><td><mark>no Brands in the list</mark></td></tr>
                                @endif

                                </tbody>
                            </table>


                        </div>
                    </div>

@endsection