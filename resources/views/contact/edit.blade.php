@extends('layout.base')
@section('content')
    <div class="row mt">
        <div class="col-md-12">
            <script>
                $('#myTabs a').click(function (e) {
                    e.preventDefault()
                    $(this).tab('show')
                })
                $('#myTabs a[href="#Addnew"]').tab('show') // Select tab by name

            </script>
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Addnew" aria-controls="Addnew" role="tab" data-toggle="tab">Add New</a></li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="Addnew">
                        <div class="form-panel">
                            <h3><i class="fa fa-angle-right"></i> Customer Info </h3>
                            <div class="col-sm-12">

                            </div>
                            {!! Form::open(['action' => ['ContactController@update',$editt->id], 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7; margin-top: 2%;">
                                {{Form::label('Type', 'Type',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::select('Type', ['Customer' => 'Customer', 'Vendor' => 'Vendor'], null, ['placeholder' => 'Pick a type...','class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style="display: none; border-bottom: 0px solid #eff2f7;">
                                {{Form::label('id', 'Business id',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Business_id',$editt->Business_id,['class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7; margin-top: 2%;">
                                {{Form::label('Name', 'Name',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Name',$editt->name,['class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Email', 'Email',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Email',$editt->email,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('City', 'City',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('City',$editt->city,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Country', 'Country',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Country',$editt->country,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Address', 'Contact',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('contact',$editt->contact,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Address', 'Address',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-6">
                                    {{Form::text('Address',$editt->address,['class'=>'form-control'])}}
                                </div>
                            </div>

                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('logo', 'Brand logo',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-6">
                                    {{Form::file('brand_logo')}}
                                </div>
                            </div>

                            {{form::hidden('_method','PUT')}}
                            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
                            {!! Form::close() !!}


                        </div>
                    </div>

@endsection