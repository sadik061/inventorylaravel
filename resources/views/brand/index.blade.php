@extends('layout.base')
@section('content')
    <h3><i class="fa fa-angle-right"></i> Brands</h3>
        <div class="col-md-6">
            <div class="content-panel">
                <h4>Brand list</h4><hr>
                <table class="table table-striped table-advance table-hover">

                    <thead>
                    <tr>
                        <th><i class="fa fa-bullhorn"></i> id</th>
                        <th class="hidden-phone"><i class="fa fa-bullhorn"></i> name</th>

                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($Brand) >= 1)
                    @foreach($Brand as $b)
                        <tr>
                            <td>{{$b->id}}</td>
                            <td class="hidden-phone">{{$b->name}}</td>
                            <td>

                                {!! Form::open(['action' => ['BrandController@destroy',$b->id], 'method'=>'POST']) !!}
                                <a class="editt btn btn-primary btn-xs" href="/Brand/{{$b->id}}/edit">Edit</a>
                                {{Form::hidden('_method','DELETE')}}
                                {{form::submit('Delete',['class' => 'btn btn-danger btn-xs'])}}

                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    @else
                        <tr><td><mark>no Brands in the list</mark></td></tr>
                    @endif

                    </tbody>
                </table>
            </div>
        </div>
        <div id="div2" class="col-lg-6">
            <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Add New</h4>
                {!! Form::open(['action' => 'BrandController@store', 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
                <div class="form-group">
                    {{Form::label('title', 'Brand Name',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                    <div class="col-sm-10">
                        {{Form::text('title','',['class'=>'form-control'])}}
                        <span class="help-block">New Brand Name</span>
                    </div>
                </div>
                {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-lg-6" >
        </div>









@endsection