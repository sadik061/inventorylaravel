@extends('layout.base')
@section('content')
    <h3><i class="fa fa-angle-right"></i> Category</h3>
    <div class="col-md-6">
        <div class="content-panel">
            <h4>Category list</h4><hr>
            <table class="table table-striped table-advance table-hover">

                <thead>
                <tr>
                    <th>id</th>

                    <th>Parent Name</th>
                    <th class="hidden-phone">Category name</th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($Category) >= 1)
                    @foreach($Category as $c)
                        <tr>
                            <td>{{$c->id}}</td>
                            <?php $a=0; ?>
                            @foreach($Category as $cc)
                                @if($c->parent_id == $cc->id)
                                    <td>{{$cc->Category_name}}</td>
                                    <?php $a=1; ?>
                                @endif
                            @endforeach
                            <?php if($a==0){
                                echo "<td>None</td>";
                                }
                            ?>
                            <td class="hidden-phone">{{$c->Category_name}}</td>
                            <td>

                                {!! Form::open(['action' => ['CategoryController@destroy',$c->id], 'method'=>'POST']) !!}
                                <a class="editt btn btn-primary btn-xs" href="/category/{{$c->id}}/edit">Edit</a>
                                {{Form::hidden('_method','DELETE')}}
                                {{form::submit('Delete',['class' => 'btn btn-danger btn-xs'])}}

                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td><mark>no Brands in the list</mark></td></tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>
    <div id="div2" class="col-lg-6">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Add New</h4>
            {!! Form::open(['action' => 'CategoryController@store', 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
            <div class="form-group"  style="display: none">
                {{Form::label('Parentid', 'Paarent id',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::text('Parentid','',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Parentname', 'Parent',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::select('Parentname',$parentlist,null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Caterogy_Name', 'Caterogy Name',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::text('Caterogy_Name','',['class'=>'form-control'])}}
                </div>
            </div>
            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
            {!! Form::close() !!}
        </div>
    </div>
    <div class="col-lg-6" >
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
    function myFunction() {
    var x = document.getElementById("myText").placeholder;
    document.getElementById("demo").innerHTML = x;
    }
    </script>



@endsection