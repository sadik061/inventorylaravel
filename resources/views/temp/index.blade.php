@extends('layout.base')
@section('content')
    <div class="row mt">
        <div class="col-lg-12">
            <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Order Details</h4>
                <section id="unseen">
                    <table id="table" class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Category</th>
                            <th>Item</th>
                            <th>Brand</th>
                            <th>Location</th>
                            <th class="numeric">Price</th>

                            <th class="numeric">Stock</th>
                            <th class="numeric">amount</th>
                            <th class="numeric">discount</th>
                            <th class="numeric"style="padding-left:  2%;padding-right: 4%;">Total</th>
                            <th class="numeric">Option</th>
                        </tr>
                        </thead>
                        <tbody >
                        <tr>

                            {!! Form::open(['action' => 'TempController@store', 'method'=>'POST','class'=>'form-horizontal style-form ']) !!}
                            <td class="text">{{Form::select('cat_id',$clist,'""',['class'=>'form-control productcategory','id'=>'cat_id'])}}</td>
                            <td>
                                <select class="productname form-control" id="prod_id" name="prod_id">
                                    <option value="0" disabled="true" selected="true">choose</option>
                                </select>
                            </td>
                            <td class="text">{{Form::select('brand_id',$blist,'""',['class'=>'form-control','id'=>'brand_id'])}}</td>
                            <td>
                                <select class="location form-control" id="loca_id"  name="loca_id">
                                    <option value="0" disabled="true" selected="true">choose</option>
                                </select>
                            </td>

                            <td class="price">{{Form::text('price','',['class'=>'form-control price','disabled'])}}</td>
                            <td class="stock"><input class="form-control" name="stock" type="text" disabled id="stock"></td>
                            <td class="text">{{Form::number('amount', '0',['class'=>'form-control amount'])}}</td>
                            <td class="text">{{Form::number('discount', '0',['class'=>'form-control discount'])}}</td>
                            <td class="total"><input class="form-control total" name="total" type="text" disabled id="total"></td>


                            <td>{{form::submit('add',['class' => 'editt btn btn-default btn-s'])}}</td>
                            {!! Form::close() !!}




                        </tr>

                        </tbody>
                        <tbody id="data" >
                        @if(count($temp) >= 1)
                            @foreach($temp as $b)
                                <tr>
                                    <td>
                                        @foreach($Category as $c)
                                            @if($b->category_id==$c->id)
                                                {{$c->Category_name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>@foreach($product as $p)
                                            @if($b->item_id==$p->id)
                                                {{$p->name}}
                                            @endif
                                            @endforeach
                                    </td>
                                    <td>
                                        @foreach($Brand as $B)
                                            @if($b->brand_id==$B->id)
                                                {{$B->name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($ProductStock as $s)
                                            @if($b->sotck_id==$s->id)
                                                @foreach($Stocklist as $sl)
                                                @if($s->Stock_list_id==$sl->id)
                                                    {{$sl->stock_list_name}}
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($ProductStock as $s)
                                            @if($b->sotck_id==$s->id)
                                                        {{$s->Sale}}

                                            @endif
                                        @endforeach
                                    </td>
                                    <td></td>
                                    <td>{{$b->amount}}</td>
                                    <td>{{$b->discount}}</td>
                                    <td>
                                        @foreach($ProductStock as $s)
                                            @if($b->sotck_id==$s->id)
                                                {{$s->Sale*$b->amount-$b->discount}}

                                            @endif
                                        @endforeach
                                    </td>

                                    <td>
                                        {!! Form::open(['action' => ['TempController@destroy',$b->id], 'method'=>'POST']) !!}
                                        {{Form::hidden('_method','DELETE')}}
                                        {{form::submit('Delete',['class' => 'btn btn-danger btn-xs demo'])}}

                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr><td><mark>no Brands in the list</mark></td></tr>
                        @endif
                        </tbody>
                    </table>

                </section>
                <a class="btn btn btn-default" href="/Checkout">Checkout</a>

            </div>
        </div><!-- col-lg-12-->
    </div><!-- /row -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
           $(document).on('change','.productcategory',function(){
              var id= $(this).val();
              var div=$(this).parent().parent();
              var op="";
               $.ajax({
                  type: 'get',
                   url:'{!! URL::to('findproductname') !!}',
                   data:{'id':id},
                   success:function(data){
                       op+= '<option value="0" selected disabled>product</option>';
                       for(var i=0;i<data.length;i++){
                           op+= '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                       }
                       div.find('.productname').html(" ");
                       div.find('.productname').append(op);

                   },
                   error:function () {

                   }

               });
           });
            $(document).on('change','.productname',function(){
                var id= $(this).val();
                //console.log(id);
                var div=$(this).parent().parent();
                var op="";
                $.ajax({
                    type: 'get',
                    url:'{!! URL::to('findstock') !!}',
                    data:{'id':id},
                    success:function(data){
                       // console.log(data);
                        op+= '<option value="0" selected disabled>locationj</option>';
                        for(var i=0;i<data.length;i++){
                            op+= '<option value="'+data[i].id+'">'+data[i].stock_list_name+'</option>';
                        }
                        div.find('.location').html(" ");
                        div.find('.location').append(op);

                    },
                    error:function () {

                    }

                });
            });
            $(document).on('change','.location',function(){
                var id= $(this).val();
                var div=$(this).parent().parent();
                var product = $('#prod_id').val();
                var op="";
                var op1="";
                $.ajax({
                    type: 'get',
                    url:'{!! URL::to('findstockbylocation') !!}',
                    data:{'id':id,'p_id':product},
                    success:function(data){
                        console.log(data);
                        for(var i=0;i<data.length;i++){
                            op+= '<input class="form-control price" name="price" type="text" disabled value="'+data[i].Sale+'" id="price">';
                            op1+= '<input class="form-control Quantity" name="Quantity" type="text" disabled value="'+data[i].Quantity+'" id="Quantity">';
                        }
                        div.find('.price').text("");
                        div.find('.price').append(op);
                        div.find('.stock').text("");
                        div.find('.stock').append(op1);


                    },
                    error:function () {

                    }

                });
            });
            $(document).on('change','.amount',function(){
                var amount= $(this).val();
                var div=$(this).parent().parent();
                var op="";
                var price = $('#price').val();
                op+= '<input class="form-control total" name="total" type="text" disabled value="'+amount*price+'" id="total">';
                div.find('.total').text("");
                div.find('.total').append(op);
            });
            $(document).on('change','.discount',function(){
                var discount= $(this).val();
                var div=$(this).parent().parent();
                var op="";
                var total = $('#total').val();
                op+= '<input class="form-control total" name="total" type="text" disabled value="'+(total-discount)+'" id="total">';
                div.find('.total').text("");
                div.find('.total').append(op);
                console.log(discount);
                console.log(total);
            });
            $(document).on('click','.demo',function(){
                console.log("kaj kore");
            });
        });
    </script>

@endsection