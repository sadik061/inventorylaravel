@extends('layout.base')
@section('content')

    <div class="col-md-12">
                @if(count($Business) >= 1)
                    @foreach($Business as $b)
                        <div class="form-panel">
                            <h3><i class="fa fa-angle-right"></i> Business </h3>
                            <div class="col-sm-12">
                                <img src="/storage/brand_logo/{{$b->business_logo}}" style="height: 200px; float: right">
                            </div>

                            {!! Form::open(['action' => ['BusniessCostController@update',$b->id], 'method'=>'POST','enctype'=>'multipart/form-data','class'=>'form-horizontal style-form']) !!}
                            <div class="form-group" style="display: none; border-bottom: 0px solid #eff2f7;">
                                {{Form::label('id', 'Business id',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('id',$b->id,['class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7; margin-top: 2%;">
                                {{Form::label('Name', 'Name',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Name',$b->name,['class'=>'form-control'])}}
                                </div>

                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Email', 'Email',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Email',$b->email,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('City', 'City',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('City',$b->city,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Country', 'Country',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('Country',$b->country,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Address', 'Contact',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-4">
                                    {{Form::text('contact',$b->contact,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('Address', 'Address',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-6">
                                    {{Form::text('Address',$b->address,['class'=>'form-control'])}}
                                </div>
                            </div>

                            <div class="form-group" style=" border-bottom: 0px solid #eff2f7;">
                                {{Form::label('logo', 'Brand logo',['class'=>'col-sm-1 col-sm-1 control-label'])}}
                                <div class="col-sm-6">
                                    {{Form::file('brand_logo')}}
                                </div>
                            </div>

                            {{form::hidden('_method','PUT')}}
                            {{form::submit('Update',['class' => 'editt btn btn-primary btn-s'])}}
                            {!! Form::close() !!}
                           

                        </div>

                    @endforeach
                @else
                    <p><mark>no Brands in the list</mark></p>
                @endif

    </div>


@endsection