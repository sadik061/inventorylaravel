@extends('layout.base')
@section('content')
    <h3><i class="fa fa-angle-right"></i> Stock Places</h3>
    <div class="col-md-6">
        <div class="content-panel">
            <h4>Stock list</h4><hr>
            <table class="table table-striped table-advance table-hover">

                <thead>
                <tr>
                    <th><i class="fa fa-bullhorn"></i> id</th>
                    <th class="hidden-phone"><i class="fa fa-bullhorn"></i> name</th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($Stocklist) >= 1)
                    @foreach($Stocklist as $b)
                        <tr>
                            <td>{{$b->id}}</td>
                            <td class="hidden-phone">{{$b->stock_list_name}}</td>
                            <td>

                                {!! Form::open(['action' => ['StocklistController@destroy',$b->id], 'method'=>'POST']) !!}
                                <a class="editt btn btn-primary btn-xs" href="/stocklist/{{$b->id}}/edit">Edit</a>
                                {{Form::hidden('_method','DELETE')}}
                                {{form::submit('Delete',['class' => 'btn btn-danger btn-xs'])}}

                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td><mark>no Stock in the list</mark></td></tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>
    <div id="div2" class="col-lg-6">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Add New</h4>
            {!! Form::open(['action' => 'StocklistController@store', 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
            <div class="form-group">
                {{Form::label('title', 'Stock Name',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::text('title','',['class'=>'form-control'])}}
                    <span class="help-block">New Stock Name</span>
                </div>
            </div>
            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
            {!! Form::close() !!}
        </div>
    </div>
    <div class="col-lg-6" >
    </div>
    <div id="div1" class="col-lg-6">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Edit</h4>
            {!! Form::open(['action' => ['StocklistController@update',$editt->id], 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
            <div class="form-group">
                {{Form::label('editid', 'Stock id',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::text('editid',$editt->id,['class'=>'form-control','disabled'=>'""'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('edittitle', 'Stock Name',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::text('edittitle',$editt->stock_list_name,['class'=>'form-control'])}}
                </div>
            </div>
            {{form::hidden('_method','PUT')}}
            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
            {!! Form::close() !!}
        </div>
    </div>


@endsection