@extends('layout.base')
@section('content')
    <h3><i class="fa fa-angle-right"></i> Product</h3>
    <div class="col-md-6">
        <div class="content-panel">
            <h4>Product list</h4><hr>
            <table class="table table-striped table-advance table-hover">

                <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Brand</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($Product) >= 1)
                    @foreach($Product as $c)
                        <tr>
                            <td>{{$c->id}}</td>
                            <td class="hidden-phone">{{$c->name}}</td>
                            @foreach($Category as $cc)
                                @if($c->Category_id == $cc->id)
                                    <td>{{$cc->Category_name}}</td>
                                    <?php $a=1; ?>
                                @endif
                            @endforeach
                            @foreach($Brand as $cc)
                                @if($c->Brand_id == $cc->id)
                                    <td>{{$cc->name}}</td>
                                    <?php $a=1; ?>
                                @endif
                            @endforeach
                            <td>

                                {!! Form::open(['action' => ['ProductController@destroy',$c->id], 'method'=>'POST']) !!}
                                <a class="editt btn btn-primary btn-xs" href="/product/{{$c->id}}/edit">Edit</a>
                                {{Form::hidden('_method','DELETE')}}
                                {{form::submit('Delete',['class' => 'btn btn-danger btn-xs'])}}

                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td><mark>no Brands in the list</mark></td></tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>
    <div id="div2" class="col-lg-6">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Add New</h4>
            {!! Form::open(['action' => 'ProductController@store', 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
            <div class="form-group">
                {{Form::label('category', 'Category',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::select('category',$clist,'""',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Brand', 'Brand',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::select('Brand',$blist,'""',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Stock', 'Stock',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::select('Stock',$slist,'""',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Product', 'Product Name',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::text('Product','',['class'=>'form-control'])}}
                </div>
            </div>
            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
            {!! Form::close() !!}
        </div>
    </div>
    <div class="col-lg-6" >
    </div>
    <div id="div1" class="col-lg-6">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Edit</h4>
            {!! Form::open(['action' => ['ProductController@update',$editt->id], 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
            <div class="form-group">
                {{Form::label('editid', 'product id',['class'=>'col-sm-2 col-sm-2 control-label','disabled'=>'""'])}}
                <div class="col-sm-10">
                    {{Form::text('editid',$editt->id,['class'=>'form-control','disabled'=>'""'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('category', 'Category',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::select('category',$clist,'""',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Brand', 'Brand',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::select('Brand',$blist,'""',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Stock', 'Stock',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::select('Stock',$slist,'""',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Product', 'Product Name',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::text('Product',$editt->name,['class'=>'form-control'])}}
                </div>
            </div>
            {{form::hidden('_method','PUT')}}
            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
            {!! Form::close() !!}
        </div>
    </div>

@endsection