
    <aside>
        <div id="sidebar"  class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">

                <li class="mt" >
                    <a  class="{{ Request::is('dashboard')? 'active' :null}}" href="/dashboard">
                        <i class="fa fa-dashboard" ></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a  class="{{ Request::is('contact')? 'active' :null}}" href="/contact">
                        <i class="fa fa-tasks"></i>
                        <span>Contacts</span>
                    </a>

                </li>

                <li class="sub-menu">
                    <a id="active" href="javascript:;">
                        <i class="fa fa-bars"></i>
                        <span>Product</span>
                    </a>
                    <ul class="sub">
                        <li class="{{ Request::is('Brand')? 'active' :null}}"><a  href="/Brand" >Brand</a></li>
                        <li class="{{ Request::is('category')?'active':null}}"><a  href="/category">Category</a></li>
                        <li class="{{ Request::is('stocklist')?'active':null}}"><a  href="/stocklist">Stocks</a></li>
                        <li class="{{ Request::is('product')?'active':null}}"><a  href="/product">Add New Product</a></li>
                        <li class="{{ Request::is('productstock')?'active':null}}"><a  href="/productstock">Product Stock</a></li>
                    </ul>
                </li>

                <li class="sub-menu">
                    <a  class="{{ Request::is('business')? 'active' :null}}" href="/business">
                        <i class="fa fa-briefcase"></i>
                        <span>Busniess</span>
                    </a>

                </li>
                <li class="sub-menu">
                    <a class="{{ Request::is('accounts')? 'active' :null}}" href="/accounts">
                        <i class="fa fa-sitemap"></i>
                        <span>Accounts</span>
                    </a>

                </li>

                <li class="sub-menu">
                    <a class="{{ Request::is('temp')? 'active' :null}}" href="/temp">
                        <i class="fa fa-tasks"></i>
                        <span>Order</span>
                    </a>

                </li>
                <li class="sub-menu">
                    <a href="javascript:;" >
                        <i class="fa fa-th"></i>
                        <span>Invoices</span>
                    </a>
                    <ul class="sub">
                        <li><a  href="basic_table.html">Basic Table</a></li>
                        <li><a  href="responsive_table.html">Responsive Table</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" >
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Reports</span>
                    </a>
                    <ul class="sub">
                        <li><a  href="morris.html">Morris</a></li>
                        <li><a  href="chartjs.html">Chartjs</a></li>
                    </ul>
                </li>

            </ul>
            <!-- sidebar menu end-->
        </div>
        <script>
            function myFunction() {
                document.getElementById("product").classList.add('active');
            }
        </script>
    </aside>

