@extends('layout.base')
@section('content')
    <h3><i class="fa fa-angle-right"></i> Product Stock</h3>
    <div class="col-md-9">
        <div class="content-panel">
            <h4>Product Stocks</h4><hr>
            <table class="table table-striped table-advance table-hover">

                <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Brand</th>
                    <th>Location</th>
                    <th>Purchase Price</th>
                    <th>Sale Price</th>
                    <th>Quantity</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($ProductStock) >= 1)
                    @foreach($ProductStock as $c)
                        <tr>
                            <td>{{$c->id}}</td>
                            @foreach($Product as $cc)
                                @if($c->Product_id == $cc->id)
                                    <td>{{$cc->name}}</td>
                                    @foreach($Category as $ccc)
                                        @if($cc->Category_id == $ccc->id)
                                            <td>{{$ccc->Category_name}}</td>
                                            @foreach($Brand as $bb)
                                                @if($cc->Brand_id == $bb->id)
                                                    <td>{{$bb->name}}</td>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                            @foreach($Stocklist as $sl)
                                @if($c->Stock_list_id == $sl->id)
                                    <td>{{$sl->stock_list_name}}</td>
                                    <?php $a=1; ?>
                                @endif
                            @endforeach
                            <td>{{$c->Purchase}}</td>
                            <td>{{$c->Sale}}</td>
                            <td>{{$c->Quantity}}</td>
                            <td>

                                {!! Form::open(['action' => ['ProductStockController@destroy',$c->id], 'method'=>'POST']) !!}
                                <a class="editt btn btn-primary btn-xs" href="/productstock/{{$c->id}}/edit">Edit</a>
                                {{Form::hidden('_method','DELETE')}}
                                {{form::submit('Delete',['class' => 'btn btn-danger btn-xs'])}}

                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td><mark>no Brands in the list</mark></td></tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>
    <div id="div2" class="col-lg-3">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Add New</h4>
            {!! Form::open(['action' => 'ProductStockController@store', 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
            <div class="form-group">
                {{Form::label('Product', 'Product',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::select('Product',$plist,'""',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Stock', 'Stock',['class'=>'col-sm-2 col-sm-2 control-label'])}}
                <div class="col-sm-10">
                    {{Form::select('Stock',$slist,'""',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group" style="display: none">
                {{Form::label('Purchase', 'Purchase price',['class'=>'col-sm-4 col-sm-4 control-label'])}}
                <div class="col-sm-8">
                    {{Form::text('Purchase','0',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group" style="display: none">
                {{Form::label('Sale', 'Sale price',['class'=>'col-sm-4 col-sm-4 control-label'])}}
                <div class="col-sm-8">
                    {{Form::text('Sale','0',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Quantity', 'Quantity',['class'=>'col-sm-4 col-sm-4 control-label'])}}
                <div class="col-sm-8">
                    {{Form::text('Quantity','',['class'=>'form-control'])}}
                </div>
            </div>
            {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
            {!! Form::close() !!}
        </div>
    </div>

    <div class="col-lg-6" >
    </div>

@endsection