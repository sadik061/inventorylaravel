@extends('layout.base')
@section('content')
    <h3><i class="fa fa-angle-right"></i> Product Stock</h3>
    <div class="col-md-12">
        <div class="content-panel">
            <h4>Product Stocks</h4><hr>

            <table class="table table-striped table-advance table-hover">

                <thead>
                <tr>
                    {!! Form::open(['action' => ['ProductStockController@update',$editt->id], 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
                    <th>id</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Brand</th>
                    <th>Location</th>
                    <th>Purchase Price</th>
                    <th>Sale Price</th>
                    <th>Quantity</th>
                    <th>Added</th>
                    <th>Last Modified</th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                        <tr>
                            <td>{{$editt->id}}</td>
                            @foreach($Product as $cc)
                                @if($editt->Product_id == $cc->id)
                                    <td>{{$cc->name}}</td>
                                    @foreach($Category as $ccc)
                                        @if($cc->Category_id == $ccc->id)
                                            <td>{{$ccc->Category_name}}</td>
                                            @foreach($Brand as $bb)
                                                @if($cc->Brand_id == $bb->id)
                                                    <td>{{$bb->name}}</td>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                            @foreach($Stocklist as $sl)
                                @if($editt->Stock_list_id == $sl->id)
                                    <td><div style="max-width: 80%">{{Form::select('Stock',$slist,$sl->id,['class'=>'form-control'])}}</div></td>
                                    <?php $a=1; ?>
                                @endif
                            @endforeach
                            <td>{{$editt->Purchase}}</td>
                            <td>{{Form::text('Sale',$editt->Sale,['class'=>'form-control'])}}</div></td>
                            <td>{{Form::text('Quantity',$editt->Quantity,['class'=>'form-control'])}}</div></td>
                            <td>{{$editt->created_at}}</td>
                            <td>{{$editt->updated_at}}</td>
                            <td>
                                <div class="form-group" style="display: none">
                                    {{Form::text('Product',$editt->Product_id,['class'=>'form-control'])}}
                                    {{Form::text('Purchase',$editt->Purchase,['class'=>'form-control'])}}

                                </div>
                                {{form::hidden('_method','PUT')}}
                                {{form::submit('submit',['class' => 'editt btn btn-primary btn-s'])}}
                                {!! Form::close() !!}
                            </td>
                        </tr>


                </tbody>
            </table>
        </div>
    </div>

    <div class="col-lg-7" >
    </div>
    <div class="col-lg-5" style="margin-top:  2%;">
        <table>
            <h4>Add new Costs</h4>
            <thead>

            </thead>
            <tbody>
            {!! Form::open(['action' => 'ProductCostController@store', 'method'=>'POST','class'=>'form-horizontal style-form']) !!}
            <td></td>
            <div style="display: none">
                {{Form::text('product_stock_id',$editt->id,['class'=>'form-control'])}}
            </div>
            <td>{{Form::text('Name','',['class'=>'form-control','placeholder'=>'Cost Name'])}}</td>
            <td>{{Form::text('Description','',['class'=>'form-control','placeholder'=>'Description'])}}</td>
            <td>{{Form::text('Amount','',['class'=>'form-control','placeholder'=>'Amount'])}}</td>
            <td></td>
            <td>{{form::submit('Add New Cost',['class' => 'editt btn btn-primary btn-s'])}}</td>

            {!! Form::close() !!}
            </tbody>
        </table>
    </div>
    <div class="col-lg-12" >
        <div class="content-panel" style="margin-top: 2%">
            <h4>Product Costs</h4>
            <table class="table table-striped table-advance table-hover">

                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>descripttion</th>
                    <th>amount</th>
                    <th>created_at</th>
                    <th>updated_at</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                    @if(count($Cost) >= 1)


                        @foreach($Cost as $sl)
                            <tr>
                                <td>{{$sl->id}}</td>
                                <td>{{$sl->name}}</td>
                                <td>{{$sl->descripttion}}</td>
                                <td>{{$sl->amount}}</td>
                                <td>{{$sl->created_at}}</td>
                                <td>{{$sl->updated_at}}</td>
                                <td>
                                    {!! Form::open(['action' => ['ProductCostController@destroy',$sl->id], 'method'=>'POST']) !!}
                                    {{Form::hidden('_method','DELETE')}}
                                    {{form::submit('Delete',['class' => 'btn btn-danger btn-xs'])}}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach


                    @else
                        <tr><td><mark>no Cost in the list</mark></td></tr>
                    @endif

                </tbody>
    </table>
    </div>

@endsection