@extends('layout.base')
@section('content')
    <div class="row mt">
        <div class="col-lg-12">
            <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Order Details</h4>
                <section id="unseen">
                    <table id="table" class="table table-bordered table-striped table-condensed">
                        <h4><i class="fa fa-angle-right"></i> purchased products</h4>
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th class="numeric"style="padding-left:  2%;padding-right: 4%;">Price</th>

                            <th class="numeric">Quantity</th>
                            <th class="numeric">Discount</th>
                            <th class="numeric"style="padding-left:  2%;padding-right: 4%;">Total</th>
                            <th class="numeric">Option</th>
                        </tr>
                        </thead>
                        <tbody >
                        <tr>
                            <td class="text"><input type="text" id="itemname" class="form-control"></td>
                            <td class="text"><input type="text" name="category" id="category" class="form-control"></td>
                            <td class="text"><input type="text" name="Brand" id="Brand" class="form-control"></td>
                            <td class="text" ><input type="text" name="Price" id="price" class="form-control"></td>
                            <td class="numeric"><input type="text" id="quantity" class="form-control col-sm-4"></td>
                            <td class="numeric"><input type="text" id="discount" class="form-control col-sm-4"></td>
                            <td class="numeric" ><input type="text" id="total" class="form-control col-sm-4"></td>
                            <td><button id="add" class="btn btn-default">add</button></td>
                        </tr>

                        </tbody>
                        <tbody id="data" >

                        </tbody>
                    </table>

                </section>

            </div>
        </div><!-- col-lg-12-->
    </div><!-- /row -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#add").select(function () {
                $.get('getrequest', function(data) {
                    $('#data').html(data);
                });
            });

        });

    </script>

@endsection